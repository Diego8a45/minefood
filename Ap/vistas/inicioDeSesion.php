<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/mineFode/publico/css/bootstrap.min.css">
    <link rel="stylesheet" href="/mineFode/publico/css/inicioDeSesion.css">
    <title>Inicia Sesi&oacute;n</title>
</head>
<body>
    <div class="row  tabla">
        <div class="col-2 col-form-label-sm titulo">
            <a href="index.php?controller=Usuario&action=inicio" class="nombre">MINE FOOD</a>
        </div>
        <div class="col-10 col-form-label-sm">
            <ul class="lista">
                <li>
                    <a href="index.php?controller=Usuario&action=verificarLogin"><button class="boton"> Iniciar Sesi&oacute;n</button></a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=registrarUsuario" class="menu">Registrar</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=verificarLogin" class="menu">Recetas</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-3"></div>
        <div class="col-6 ">
            <div class="contenedor">
                <form method="post" action="index.php?controller=Usuario&action=verificarLogin">
                    <h3>Bienvenido para poder continuar<br>inicie sesi&oacute;n en<br>MINEFOOD</h3>
                    <label for="" class="datos">Correo: </label>
                    <input type="email" class="datosInsertados" name="email">
                    <label for="" class="datos">Contrase&ntilde;a: </label>
                    <input type="password" class="datosInsertados" name="password">
                    <input type="submit" value="Iniciar sesi&oacute;n"" class="entrar">
                </form>
            </div>
        </div>
        <div class="col-3"></div>
    </div>
</body>
</html>
