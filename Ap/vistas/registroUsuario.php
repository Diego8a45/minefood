<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/mineFode/publico/css/bootstrap.min.css">
    <link rel="stylesheet" href="/mineFode/publico/css/registrarUsuario.css">
    <title>Registrarse</title>
</head>
<body>

    <div class="row  tabla">
        <div class="col-2 col-form-label-sm titulo">
            <a href="index.php?controller=Usuario&action=inicio" class="nombre">MINE FOOD</a>
        </div>
        <div class="col-10 col-form-label-sm">
            <ul class="lista">
                <li>
                    <a href="index.php?controller=Usuario&action=verificarLogin"><button class="boton"> Iniciar Sesi&oacute;n</button></a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=registrarUsuario" class="menu">Registrar</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=verificarLogin" class="menu">Recetas</a>
                </li>
            </ul>
        </div>
    </div>

        <div class="row">
            <div class="col-4 co">
            </div>
            <div class="col-4 col-form-label-sm formulario">
                <div class="col-12 col-form-label-sm">
                    <h2>&uacute;nete a Mine-Food Hoy</h2>
                </div>
                <div class="col-form-label-sm">
                    <form method="post" action="index.php?controller=Usuario&action=registrarUsuario">
                        <label for="" class="datos">Nombre: </label>
                        <input type="text" class="datosInsertados" name="nombre">
                        <label for="" class="datos">Apellido Paterno: </label>
                        <input type="text" class="datosInsertados" name="apellidoPaterno">
                        <label for="" class="datos">Apellido Materno: </label>
                        <input type="text" class="datosInsertados" name="apellidoMaterno">
                        <label for="" class="datos">edad: </label>
                        <input type="date" class="fecha" name="fechaNacimiento">
                        <label for="" class="datos">Correo: </label>
                        <input type="email" class="correo" name="correo">
                        <label for="" class="datos">Contrase&ntilde;a: </label>
                        <input type="password" class="datosInsertados" name="contrasenia">
                        <input type="submit" value="Enviar" class="registrar">
                    </form>
                </div>

            </div>
            <div class="col -4">

            </div>
        </div>
        </div>

</body>
</html>
