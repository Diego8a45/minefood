<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/mineFode/publico/css/bootstrap.min.css">
    <link rel="stylesheet" href="/mineFode/publico/css/perfilDelUsuario.css">
    <title>Perfil</title>
</head>
<body>
    <div class="row  tabla">
        <div class="col-2 col-form-label-sm titulo">
            <a href="paginaInicial.php" class="nombre">MINE FOOD</a>
        </div>
        <div class="col-10 col-form-label-sm">
            <ul class="lista">
                <li>
                    <a href="index.php?controller=Usuario&action=salir" class="menu">salir</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=perfil" class="menu">Perfil</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=perfil" class="menu">Publicar</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row cuerpo">
        <div class="col-5"></div>
        <div class="col-2 foto">
                <img src="/mineFode/publico/imagenes/perfil.jpg" class="imagen">
        </div>
        <div class="col-5"></div>
    </div>

    <div class="row inf">
        <div class="col-12">
            <ul class="cu">
                <li>
                    <a href="index.php?controller=Usuario&action=perfil" class="op">Perfil</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=actualizarUsuario" class="op">editar</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=info" class="op">informaci&oacute;n</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=borrar" class="op">eliminar cuenta</a>
                </li>
            </ul>
        </div>
    </div>

</body>
</html>
