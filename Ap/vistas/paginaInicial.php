<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/mineFode/publico/css/bootstrap.min.css">
    <link rel="stylesheet" href="/mineFode/publico/css/paginaPrincipal.css">
    <title>Bienvenido</title>
</head>
<body>
    <div class="row  tabla">
        <div class="col-2 col-form-label-sm titulo">
            <a href="index.php?controller=Usuario&action=inicio" class="nombre">MINE FOOD</a>
        </div>
        <div class="col-10 col-form-label-sm">
            <ul class="lista">
                <li>
                    <a href="index.php?controller=Usuario&action=verificarLogin"><button class="boton"> Iniciar Sesi&oacute;n</button></a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=registrarUsuario" class="menu">Registrar</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=verificarLogin" class="menu">Recetas</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-1"></div>
        <div class="col-10 contenedor">
            <div class="row bienvenida">
                <div class="col-6">
                    <h2>BIENVENIDO A MINE-FOOD</h2>
                    <h5>Podras encontrar cualquier articulo relacionado a la cocina, asi como subir productos, recetas y tener tus propios seguidores</h5>
                    <br>
                    <h5>Que esperas para entrar...</h5>
                </div>
                <div class="col-5 imagen">
                </div>
            </div>
        </div>
        <div class="col-1"> </div>
    </div>
</body>
</html>