<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/mineFode/publico/css/bootstrap.min.css">
    <link rel="stylesheet" href="/mineFode/publico/css/editarInformacion.css">
    <title>Editar Datos</title>
</head>
<body>
    <div class="row  tabla">
        <div class="col-2 col-form-label-sm titulo">
            <a href="paginaInicial.php" class="nombre">MINE FOOD</a>
        </div>
        <div class="col-10 col-form-label-sm">
            <ul class="lista">
                <li>
                    <a href="" class="menu">salir</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=perfil" class="menu">Perfil</a>
                </li>
                <li>
                    <a href="index.php?controller=Usuario&action=perfil" class="menu">Publicar</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-3 co">

        </div>
        <div class="col-6 col-form-label-sm formulario">

            <div class="col-12 col-form-label-sm">
                <h2>Editar informaci&oacute;n</h2>
            </div>
            <div class="col-form-label-sm">
                <form method="post" action="index.php?controller=Usuario&action=actualizarUsuario">
                    <label for="" class="datos">ID: <?php echo $_SESSION["id"]; ?> </label>
                    <label for="" class="datos">Nombre: </label>
                    <input type="text" class="datosInsertados" name="nombre" value="<?php echo $_SESSION["nom"];?>">
                    <label for="" class="datos">Apellido Paterno: </label>
                    <input type="text" class="datosInsertados" name="apellidoPaterno" value="<?php echo $_SESSION["ap"];?>">
                    <label for="" class="datos">Apellido Materno: </label>
                    <input type="text" class="datosInsertados" name="apellidoMaterno" value="<?php echo$_SESSION["am"];?>">
                    <label for="" class="datos">edad: </label>
                    <input type="date" class="fecha" name="fechaNacimiento" value="<?php echo $_SESSION["fn"];?>">
                    <label for="" class="datos">Correo: </label>
                    <input type="email" class="correo" name="correo" value="<?php echo $_SESSION["correo"];?>">
                    <label for="" class="datos">Contrase&ntilde;a: </label>
                    <input type="password" class="datosInsertados" name="contrasenia" value="<?php echo $_SESSION["pass"];?>">
                    <input type="submit" value="Enviar" class="registrar">
                </form>
            </div>

        </div>
        <div class="col-3">

        </div>
    </div>
</div>

</body>
</html>