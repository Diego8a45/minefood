<?php

namespace mine;
class Usuario extends conex
{
    public $id;
    public $id_usuario;
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $fechaDeNacimiento;
    public $correo;
    public $contrasenia;

    public function __construct()
    {
        parent::__construct();
    }

    public function crear(){
        $prep=mysqli_prepare($this->conexion, "INSERT INTO usuario(nombre, apellido_paterno, apellido_materno, fecha_nacimiento, correo, contrasenia) VALUES (?,?,?,?,?,?)");
        $prep->bind_param("ssssss", $this->nombre, $this->apellidoPaterno, $this->apellidoMaterno, $this->fechaDeNacimiento,$this->correo,$this->contrasenia);
        $prep->execute();
    }

    static function login($correo, $contrasenia)
    {
        $conexion = new conex();
        $pre = mysqli_prepare($conexion->conexion, "SELECT * FROM usuario WHERE correo = ? and contrasenia=?");
        $pre->bind_param("ss", $correo,$contrasenia);
        $pre->execute();
        $resultado=$pre->get_result();
        return $resultado->fetch_object();
    }

    function actualizar()
    {
        $pre = mysqli_prepare($this->conexion, "UPDATE usuario SET nombre=?,apellido_paterno=?,apellido_materno=?,fecha_nacimiento=?,correo=?,contrasenia=? WHERE id=?");
        $pre->bind_param("ssssssi", $this->nombre, $this->apellidoPaterno, $this->apellidoMaterno, $this->fechaDeNacimiento, $this->correo, $this->contrasenia,$this->id_usuario);
        $pre->execute();
    }

    static function eliminar($id){
        $ne=new conex();
        $pre=mysqli_prepare($ne->conexion, "DELETE FROM usuario WHERE id=?");
        $pre->bind_param("s", $id);
        $pre->execute();
        return true;
}

    public function mostrarTodo(){

}
}