<?php
require "Ap/modelos/conex.php";
require "Ap/modelos/Usuario.php";

use conexion\conex;
use mine\Usuario;

class UsuarioController
{
    public function perfil()
    {
        require 'Ap/vistas/perfilDeUsuario.php';
    }
    public function inicio()
    {
        require 'Ap/vistas/paginaInicial.php';
    }
    public function usuario()
    {
        require 'Ap/vistas/registroUsuario.php';
    }

    public function registrarUsuario()
    {
        require 'Ap/vistas/registroUsuario.php';
        if ((isset($_POST["nombre"])) || (isset($_POST["apellidoPaterno"])) || (isset($_POST["apellidoMaterno"])) || (isset($_POST["fechaNacimiento"])) || (isset($_POST["correo"])) || (isset($_POST["contrasenia"]))) {
            $usuario = new Usuario();
            $usuario->nombre = $_POST["nombre"];
            $usuario->apellidoPaterno = $_POST["apellidoPaterno"];
            $usuario->apellidoMaterno = $_POST["apellidoMaterno"];
            $usuario->fechaDeNacimiento = $_POST["fechaNacimiento"];
            $usuario->correo = $_POST["correo"];
            $usuario->contrasenia = $_POST["contrasenia"];
            $usuario->crear();
                echo "<center><font color='white' size='10'>Registrado Correctamente</font></center>";
        }
        else{
            echo "<center><font color='white' size='10'>Ingrese datos Correctos</font></center>";
        }

    }


    public function loginVista()
    {
        require "Ap/vistas/inicioDeSesion.php";
    }


    function verificarLogin()
    {
        if((isset($_POST["email"])) || (isset($_POST["password"]))){

            $correo = $_POST["email"];
            $password = $_POST["password"];
            $usuario = usuario::login($correo, $password);
            if($usuario){
                require "Ap/vistas/perfilDeUsuario.php";
                $_SESSION["id"]=$usuario->id;
                $_SESSION["nom"]=$usuario->nombre;
                $_SESSION["ap"]=$usuario->apellido_paterno;
                $_SESSION["am"]=$usuario->apellido_materno;
                $_SESSION["fn"]=$usuario->fecha_nacimiento;
                $_SESSION["correo"]=$usuario->correo;
                $_SESSION["pass"]=$usuario->contrasenia;
            }
            else{
                require "Ap/vistas/inicioDeSesion.php";
                echo "<center><font color='white' size='10'>Ingrese datos Correctos</font></center>";
            }
        }
        else{
            require "Ap/vistas/inicioDeSesion.php";
            echo "<center><font color='white' size='10'>Ingrese datos</font></center>";
        }

    }
    public function info(){
        require "Ap/vistas/infUsuario.php";
        $_SESSION["id"];
        $_SESSION["nom"];
        $_SESSION["ap"];
        $_SESSION["am"];
        $_SESSION["fn"];
        $_SESSION["correo"];
        $_SESSION["pass"];
    }

    public function actualizarUsuario()
    {
        require "Ap/vistas/editarDatos.php";
        if ((isset($_POST["nombre"])) || (isset($_POST["apellidoPaterno"])) || (isset($_POST["apellidoMaterno"])) || (isset($_POST["fechaNacimiento"])) || (isset($_POST["correo"])) || (isset($_POST["contrasenia"]))) {
            $usuario = new Usuario();
            $usuario->id_usuario=$_SESSION["id"];
            $usuario->nombre = $_POST["nombre"];
            $usuario->apellidoPaterno = $_POST["apellidoPaterno"];
            $usuario->apellidoMaterno = $_POST["apellidoMaterno"];
            $usuario->fechaDeNacimiento = $_POST["fechaNacimiento"];
            $usuario->correo = $_POST["correo"];
            $usuario->contrasenia = $_POST["contrasenia"];
            $usuario->actualizar();
        }
    }
    public function salir(){
        require "Ap/vistas/paginaInicial.php";

    }
    function borrar(){
        $id=$_SESSION["id"];
        $usuario = usuario::eliminar($id);
        if($usuario){
            require 'Ap/vistas/paginaInicial.php';
        }

    }


}